**CONSIDERACIONES**

** ENDPOINTS **
Para el caso del endpoint de login se tuvo las siguientes consideraciones:

El usuario va a estar parametrizado en el application.properties
y va a ser grabado en la base de datos h2 en el metodo init de la clase UserServiceImpl
username:admin password:admin


Para el caso del endpoint de provincias se tuvo las siguientes consideraciones:
- El parámetro nombre no es requerido por lo que en el caso de no venir o que tenga valor blanco trae una lista con todas las provincias
- Si el parámetro es enviado retorna una lista de provincias con aquellas que se encuentren.

-----------------------------------------------------------------------------------------------------------------------
** Proyecto **
* Para el proyecto se utilizo spring-security  *
- La configuración está en la clases SecurityConfig
- El usuario para la autenticación es username:admin password:admin

Se utilizó cache en el endpoint de provincias.
- La configuración está en la clase CacheConfig 


Para la documentación se utilizó Swagger
