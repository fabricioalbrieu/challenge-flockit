package com.flockit.challenge.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.flockit.challenge.application.rest.dto.ProvinciaResponseDTO;
import com.flockit.challenge.rest.service.RestClientService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/provincias")
public class ProvinciaController {

	@Autowired
	private RestClientService serviceRest;

	@GetMapping
	@ApiOperation(value = "Retorna latitud y longitud de la provincia", response = ProvinciaResponseDTO.class)
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Provincia encontrada"),
			@ApiResponse(code = 204, message = "No se encontró la provincia"),
			@ApiResponse(code = 404, message = "Bad request") })
	public ResponseEntity<List<ProvinciaResponseDTO>> getProvincia(@RequestParam(value = "nombre", required = false) String nombre) {
		
		return new ResponseEntity<List<ProvinciaResponseDTO>>(serviceRest.getProvincias(nombre), HttpStatus.OK);
	}
}
