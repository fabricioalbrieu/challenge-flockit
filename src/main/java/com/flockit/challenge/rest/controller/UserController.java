package com.flockit.challenge.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.flockit.challenge.application.model.User;
import com.flockit.challenge.rest.service.UserService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Controller
public class UserController {
	
	@Autowired
	private UserService userService;

	@PostMapping("/login")
	@ApiOperation(value = "Login")
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Login exitoso"),
			@ApiResponse(code = 403, message = "Contraseña incorrecta"),
			@ApiResponse(code = 404, message = "Bad request") })
	public ResponseEntity<Void> login(@RequestBody(required = true) User user) {
		userService.login(user);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
}
