package com.flockit.challenge.rest.service;

import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.flockit.challenge.application.config.UserAdmin;
import com.flockit.challenge.application.entitys.UserEntity;
import com.flockit.challenge.application.model.User;
import com.flockit.challenge.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private UserAdmin userParam;

	@PostConstruct
	public void init() {
		String hashedPW = BCrypt.hashpw(userParam.getPassword(), BCrypt.gensalt());
		userRepository.save(new UserEntity(userParam.getUsername(), hashedPW));
	}

	public Boolean login(User user) {

		Optional<UserEntity> optional = userRepository.findByUsername(user.getUsername());

		if (optional.isPresent()) {
			UserEntity userEntity = optional.get();
			if (passwordEncoder.matches(user.getPassword(), userEntity.getPassword())) {
				return Boolean.TRUE;
			} else {
				throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Contraseña incorrecta");
			}

		} else {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND,
					String.format("Usuario %s no encontrado", user.getUsername()));
		}

	}

}
