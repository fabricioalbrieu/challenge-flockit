package com.flockit.challenge.rest.service;

import java.util.List;

import com.flockit.challenge.application.rest.dto.ProvinciaResponseDTO;

public interface RestClientService {
	
	List<ProvinciaResponseDTO> getProvincias(final String nombre);

}
