package com.flockit.challenge.rest.service;

import com.flockit.challenge.application.model.User;

public interface UserService {

	public Boolean login(User user);
}
