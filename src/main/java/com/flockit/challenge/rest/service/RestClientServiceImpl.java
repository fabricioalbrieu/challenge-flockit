package com.flockit.challenge.rest.service;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;

import com.flockit.challenge.application.rest.dto.Provincia;
import com.flockit.challenge.application.rest.dto.ProvinciaDTO;
import com.flockit.challenge.application.rest.dto.ProvinciaResponseDTO;

@Service
public class RestClientServiceImpl implements RestClientService {

	private static final Logger logger = LoggerFactory.getLogger(RestClientServiceImpl.class);
	private static final String GET_PROVINCIAS = "https://apis.datos.gob.ar/georef/api/provincias";

	@Autowired
	RestTemplate restTemplate;

	@Cacheable("provincias")
	public List<ProvinciaResponseDTO> getProvincias(String nombre) {
		logger.info("RestClientServiceImpl - callGetPronviciaApi - inicio");
		HttpHeaders headers = new HttpHeaders();
		URI targetUrl;
		UriComponentsBuilder uri = UriComponentsBuilder.fromUriString(GET_PROVINCIAS);
		if(nombre != null && !nombre.equalsIgnoreCase("")) {
			targetUrl =  uri.queryParam("nombre", nombre).build().encode().toUri();
		}else {
			targetUrl = uri.build().encode().toUri();
		}

		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

		ProvinciaDTO response = restTemplate.getForObject(targetUrl, ProvinciaDTO.class);

		Optional<List<ProvinciaResponseDTO>> respuesta = !response.getProvincias().isEmpty()
				? Optional.of(convertToResponse(response.getProvincias()))
				: Optional.ofNullable(null);

		return respuesta.orElseThrow(() -> new ResponseStatusException(HttpStatus.NO_CONTENT,
				String.format("Provincia %s no encontrado", nombre)));
	}

	private List<ProvinciaResponseDTO> convertToResponse(List<Provincia> provincia) {

		List<ProvinciaResponseDTO> provincias = new ArrayList<ProvinciaResponseDTO>();
		provincias = provincia.stream().map(prov -> new ProvinciaResponseDTO(prov.nombre,
				prov.getCentroide().getLat(), prov.getCentroide().getLon())).collect(Collectors.toList());
		return provincias;

	}
}
