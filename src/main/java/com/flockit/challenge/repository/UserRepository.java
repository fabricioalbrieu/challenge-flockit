package com.flockit.challenge.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.flockit.challenge.application.entitys.UserEntity;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Integer>{

	
	public Optional<UserEntity> findByUsernameAndPassword(String username, String password);
	
	public Optional<UserEntity> findByUsername(String username);

			
}
