package com.flockit.challenge.application.rest.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@lombok.Data
@lombok.NoArgsConstructor
@lombok.EqualsAndHashCode
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "cantidad", "inicio", "parametros", "provincias", "total" })
@Generated("jsonschema2pojo")
public class ProvinciaDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3063109112122298365L;
	@JsonProperty("cantidad")
	public Integer cantidad;
	@JsonProperty("inicio")
	public Integer inicio;
	@JsonProperty("parametros")
	public Parametros parametros;
	@JsonProperty("provincias")
	public List<Provincia> provincias = null;
	@JsonProperty("total")
	public Integer total;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}