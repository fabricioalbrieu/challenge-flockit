package com.flockit.challenge.application.rest.dto;

import org.springframework.stereotype.Component;

@lombok.Data
@lombok.NoArgsConstructor
@lombok.EqualsAndHashCode

@Component
public class ProvinciaResponseDTO {
	private String provincia;
	private String lat;
	private String lon;

	
	public ProvinciaResponseDTO(String provincia, Double lat, Double lon) {
		this.provincia = provincia;
		this.lat =lat.toString();
		this.lon= lon.toString();
	}

}
