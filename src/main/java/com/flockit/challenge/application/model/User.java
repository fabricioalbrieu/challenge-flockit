package com.flockit.challenge.application.model;

import org.springframework.stereotype.Component;

@lombok.Data
@lombok.NoArgsConstructor
@lombok.EqualsAndHashCode

@Component
public class User {

	private String username;
	private String password;

}
