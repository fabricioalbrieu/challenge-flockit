package com.flockit.challenge.application.entitys;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@lombok.Data
@lombok.NoArgsConstructor
@lombok.EqualsAndHashCode
@Entity
@Table(name="user")
public class UserEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Integer id;
	
	@Column(name="user_name")
	private String username;
	
	@Column(name="password")
	private String password;

	public UserEntity(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}



}
