package com.flockit.challenge.application.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@lombok.Data
@lombok.NoArgsConstructor
@lombok.EqualsAndHashCode
@ConfigurationProperties(prefix= "user")
@Component
public class UserAdmin {
	
	private String username;
	private String password;

}
